<?php

return [

    'prismic' => [
        'blog' => [
            'documentType'    => 'article',
            'defaultOrder'    => 'published',
            'pubDate'         => 'published',
            'config-bookmark' => 'blog-config',
            'meta-data-file' => __DIR__ . '/../data/meta.json',
        ],
    ],

    'router' => [
        'routes' => [
            'prismic-blog' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/blog',
                    'defaults' => [
                        'controller' => 'NetgluePrismicBlog\Mvc\Controller\BlogController',
                        'action' => 'index',
                        'view' => 'prismic-blog/blog-index',
                        'bookmark' => 'blog-index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'paged' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/page/:page',
                            'defaults' => [

                            ],
                        ],
                        'constraints' => [
                            'page' => '[0-9]+',
                        ],
                    ],
                    'article' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/article/:uid',
                            'defaults' => [
                                'mask' => 'article',
                                'bookmark' => null,
                                'action' => 'article',
                                'view' => 'prismic-blog/article',
                            ],
                        ],
                    ],
                    'tag' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/tag/:tag[/page/:page]',
                            'defaults' => [
                                'bookmark' => 'blog-tags',
                                'action' => 'tag',
                                'view' => 'prismic-blog/archives',
                            ],
                        ],
                        'constraints' => [
                            'page' => '[0-9]+',
                        ],
                    ],
                    'archives' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/archives/:year/:month[/page/:page]',
                            'defaults' => [
                                'bookmark' => 'blog-archives',
                                'year' => null,
                                'month' => null,
                                'action' => 'archives',
                                'view' => 'prismic-blog/archives',
                            ],
                            'constraints' => [
                                'year' => '[0-9]{4}',
                                'month' => '[0-9]{1,2}',
                                'page' => '[0-9]+',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],


    'view_manager' => [
        'template_map' => [

            /**
             * Pages
             */
            'prismic-blog/blog-index'        => __DIR__ . '/../view/pages/blog-index.phtml',
            'prismic-blog/archives'          => __DIR__ . '/../view/pages/archives.phtml',
            'prismic-blog/article'           => __DIR__ . '/../view/pages/article.phtml',

            /**
             * Partials
             */
            'prismic-blog/excerpt'           => __DIR__ . '/../view/partial/excerpt.phtml',
            'prismic-blog/article-list'      => __DIR__ . '/../view/partial/article-list.phtml',
            'prismic-blog/archive-list'      => __DIR__ . '/../view/partial/archive-list.phtml',
            'prismic-blog/article-tags'      => __DIR__ . '/../view/partial/article-tags.phtml',
            'prismic-blog/article-tag-list'  => __DIR__ . '/../view/partial/article-tag-list.phtml',
            'prismic-blog/blog-tag-list'     => __DIR__ . '/../view/partial/blog-tag-list.phtml',
            'prismic-blog/article-next-prev' => __DIR__ . '/../view/partial/article-next-prev.phtml',
            'prismic-blog/disqus-comments'   => __DIR__ . '/../view/partial/disqus-comments.phtml',
            'prismic-blog/related-articles'  => __DIR__ . '/../view/partial/related-articles.phtml',
            'prismic-blog/article-meta'      => __DIR__ . '/../view/partial/article-meta.phtml',
            'prismic-blog/article-pager'     => __DIR__ . '/../view/partial/article-pager.phtml',
        ],
    ],

    'console' => [
	    'router' => [
	        'routes' => [
	            'prismic-blog/rebuild-archives' => [
	                'options' => [
	                    'route' => 'blog rebuild-archives',
	                    'defaults' => [
	                        'controller' => 'NetgluePrismicBlog\Mvc\Controller\ConsoleController',
	                        'action' => 'rebuild-archives',
	                    ],
	                ],
	            ],
	            'prismic-blog/rebuild-tags' => [
	                'options' => [
	                    'route' => 'blog rebuild-tags',
	                    'defaults' => [
	                        'controller' => 'NetgluePrismicBlog\Mvc\Controller\ConsoleController',
	                        'action' => 'rebuild-tags',
	                    ],
	                ],
	            ],
	        ],
	    ],
	],

];
