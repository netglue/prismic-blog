# Prismic Blog

## Route Config

You need to make sure that a bookmark is setup for the blog index page. By default the bookmark is expected to be named `blog-index` but this can be anything you like if you change the route named `prismic-blog` in a local config file.

## Cron Jobs

Due to the lack of data that's returned by the prismic webhook, it's probably impractical to re-generate the archives everytime something changes as it could take a long time on large sites and currently we'd end up with problems on file locking etc, so, run a cron job to rebuild the archive dates as often as necessary. Maybe once per day?

```
$ htdocs/index.php blog rebuild-archives
```
