<?php

namespace NetgluePrismicBlog\View\Helper;

use NetgluePrismicBlog\Service\ArticleServiceAwareInterface;
use NetgluePrismicBlog\Service\ArticleServiceAwareTrait;
use Prismic\Document;
use Zend\View\Helper\AbstractHelper;

class NextArticle extends AbstractHelper implements ArticleServiceAwareInterface
{
    use ArticleServiceAwareTrait;

    public function __invoke(Document $document)
    {
        return $this->getArticleService()->getNewer($document);
    }
}
