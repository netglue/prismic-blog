<?php

namespace NetgluePrismicBlog\View\Helper;

use NetgluePrismicBlog\Service\ArticleServiceAwareInterface;
use NetgluePrismicBlog\Service\ArticleServiceAwareTrait;
use Prismic\Document;
use Zend\View\Helper\AbstractHelper;

class PreviousArticle extends AbstractHelper implements ArticleServiceAwareInterface
{
    use ArticleServiceAwareTrait;

    public function __invoke(Document $document)
    {
        return $this->getArticleService()->getOlder($document);
    }
}
