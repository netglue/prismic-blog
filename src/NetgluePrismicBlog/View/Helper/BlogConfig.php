<?php

namespace NetgluePrismicBlog\View\Helper;

use NetgluePrismicBlog\Service\BlogConfig as Config;
use NetgluePrismicBlog\Service\BlogConfigAwareTrait;
use Zend\View\Helper\AbstractHelper;
use Prismic\Document;

class BlogConfig extends AbstractHelper
{
    use BlogConfigAwareTrait;

    /**
     * @param  Config $config
     * @return void
     */
    public function __construct(Config $config)
    {
        $this->setBlogConfig($config);
    }

    /**
     * Proxy to site-wide config
     */
    public function __call($method, $args)
    {
        return call_user_func_array(array($this->getBlogConfig(), $method), $args);
    }

    /**
     * Whether to display post tags
     * @return bool
     */
    public function enableTags()
    {
        $enable = $this->get('show_post_tags');
        $enable = $enable ? strtolower($enable) : 'no';
        return $enable === 'yes';
    }

    public function commentPossible()
    {
        $shortname = $this->get('disqus_shortname');
        return !empty($shortname);
    }

    public function enableCommentCounts(Document $document = null)
    {
        if(!$this->commentPossible()) {
            return false;
        }
        $enable = $this->get('show_comment_count');
        $enable = $enable ? strtolower($enable) : 'no';
        $defaultEnable = ($enable === 'yes');

        if($defaultEnable && !is_null($document)) {
            return ($defaultEnable && $this->enableComments($document));
        }

        return $defaultEnable;
    }

    public function enableComments(Document $document = null)
    {
        if(!$this->commentPossible()) {
            return false;
        }
        $defaultEnable = $this->get('default_enable_comments')
                       ? strtolower($this->get('default_enable_comments'))
                       : 'no';
        $defaultEnable = ($defaultEnable === 'yes');

        if(null === $document) {
            return $defaultEnable;
        }

        $frag = $document->get('article.enable_comments');
        if($frag) {
            $value = strtolower($document->get('article.enable_comments')->asText());
            if($value === 'yes') {
                return true;
            }
            if($value === 'no') {
                return false;
            }
        }

        return $defaultEnable;
    }

    public function enableArchives()
    {
        $enable = $this->get('show_archives')
                ? strtolower($this->get('show_archives'))
                : 'yes';
        return ($enable === 'yes');
    }

    public function enableTagCloud()
    {
        $enable = $this->get('show_tag_cloud')
                ? strtolower($this->get('show_tag_cloud'))
                : 'yes';
        return ($enable === 'yes');
    }

}
