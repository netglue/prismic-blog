<?php

namespace NetgluePrismicBlog\View\Helper;

use NetgluePrismicBlog\Service\MetaDataService;
use Zend\View\Helper\AbstractHelper;
use DateTime;

class ArchiveList extends AbstractHelper
{
    private $metaService;

    public function __construct(MetaDataService $service)
    {
        $this->metaService = $service;
    }

    public function __invoke()
    {
        return $this;
    }

    public function getArchiveDates($asObject = false)
    {
        if($asObject) {
            return $this->getArchiveDateObjects();
        }
        return $this->metaService->getArchiveDates();
    }

    public function getArchiveDateObjects()
    {
        $dates = [];
        foreach($this->metaService->getArchiveDates() as $year => $months) {
            foreach($months as $month => $count) {
                $date = new DateTime;
                $date->setDate($year, $month, 1);
                $date->setTime(12, 0, 0);
                $dates[] = $date;
            }
        }
        return $dates;
    }
}
