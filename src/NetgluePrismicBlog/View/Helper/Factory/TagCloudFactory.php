<?php

namespace NetgluePrismicBlog\View\Helper\Factory;

use NetgluePrismicBlog\View\Helper\TagCloud;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TagCloudFactory implements FactoryInterface
{
    /**
     * Return TagCloud Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return TagCloud
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();

        $plugin = new TagCloud($serviceLocator->get('NetgluePrismicBlog\Service\MetaDataService'));

        return $plugin;
    }
}
