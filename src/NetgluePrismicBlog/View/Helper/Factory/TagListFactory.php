<?php

namespace NetgluePrismicBlog\View\Helper\Factory;

use NetgluePrismicBlog\View\Helper\TagList;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class TagListFactory implements FactoryInterface
{
    /**
     * Return TagList Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return TagList
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();

        $plugin = new TagList(
            $serviceLocator->get('NetgluePrismicBlog\Service\MetaDataService'),
            $serviceLocator->get('NetgluePrismicBlog\Service\BlogConfig')
        );

        return $plugin;
    }
}
