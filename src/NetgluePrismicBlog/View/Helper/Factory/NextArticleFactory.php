<?php

namespace NetgluePrismicBlog\View\Helper\Factory;

use NetgluePrismicBlog\View\Helper\NextArticle;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class NextArticleFactory implements FactoryInterface
{
    /**
     * Return NextArticle Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return NextArticle
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();

        $plugin = new NextArticle;
        $plugin->setArticleService($serviceLocator->get('NetgluePrismicBlog\Service\ArticleService'));

        return $plugin;
    }
}
