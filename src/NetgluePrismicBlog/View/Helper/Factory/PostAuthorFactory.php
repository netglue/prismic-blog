<?php

namespace NetgluePrismicBlog\View\Helper\Factory;

use NetgluePrismicBlog\View\Helper\PostAuthor;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PostAuthorFactory implements FactoryInterface
{
    /**
     * Return PostAuthor Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return PostAuthor
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();

        $plugin = new PostAuthor;
        $plugin->setContext($serviceLocator->get('NetgluePrismic\Context'));
        $plugin->setLinkResolver($serviceLocator->get('NetgluePrismic\Mvc\LinkResolver'));

        return $plugin;
    }
}
