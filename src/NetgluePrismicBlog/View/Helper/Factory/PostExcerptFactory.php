<?php

namespace NetgluePrismicBlog\View\Helper\Factory;

use NetgluePrismicBlog\View\Helper\PostExcerpt;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PostExcerptFactory implements FactoryInterface
{
    /**
     * Return PostExcerpt Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return PostExcerpt
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();

        $plugin = new PostExcerpt;
        $plugin->setBlogConfig($serviceLocator->get('NetgluePrismicBlog\Service\BlogConfig'));
        $plugin->setLinkResolver($serviceLocator->get('NetgluePrismic\Mvc\LinkResolver'));

        return $plugin;
    }
}
