<?php

namespace NetgluePrismicBlog\View\Helper\Factory;

use NetgluePrismicBlog\View\Helper\ArchiveList;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ArchiveListFactory implements FactoryInterface
{
    /**
     * Return ArchiveList Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return ArchiveList
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();

        $plugin = new ArchiveList($serviceLocator->get('NetgluePrismicBlog\Service\MetaDataService'));

        return $plugin;
    }
}
