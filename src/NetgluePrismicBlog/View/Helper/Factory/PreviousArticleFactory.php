<?php

namespace NetgluePrismicBlog\View\Helper\Factory;

use NetgluePrismicBlog\View\Helper\PreviousArticle;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PreviousArticleFactory implements FactoryInterface
{
    /**
     * Return PreviousArticle Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return PreviousArticle
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();

        $plugin = new PreviousArticle;
        $plugin->setArticleService($serviceLocator->get('NetgluePrismicBlog\Service\ArticleService'));

        return $plugin;
    }
}
