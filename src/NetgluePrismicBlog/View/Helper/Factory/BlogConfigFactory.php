<?php

namespace NetgluePrismicBlog\View\Helper\Factory;

use NetgluePrismicBlog\View\Helper\BlogConfig;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BlogConfigFactory implements FactoryInterface
{
    /**
     * Return BlogConfig Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return BlogConfig
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();

        $plugin = new BlogConfig($serviceLocator->get('NetgluePrismicBlog\Service\BlogConfig'));

        return $plugin;
    }
}
