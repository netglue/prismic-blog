<?php
namespace NetgluePrismicBlog\View\Helper;

use Prismic\Document;
use Prismic\Fragment\Link\DocumentLink;
use Prismic\Fragment\Link\LinkInterface;
use Zend\View\Helper\AbstractHelper;
use NetgluePrismic\ContextAwareTrait;
use NetgluePrismic\ContextAwareInterface;
use NetgluePrismicDefaults\Common\LinkResolverAwareTrait;

class PostAuthor extends AbstractHelper implements ContextAwareInterface
{

    use SingleFragmentTrait;
    use ContextAwareTrait;
    use LinkResolverAwareTrait;

    public function __construct()
    {
        $this->setFragmentName('author');
    }

    /**
     * @param Document $document
     * @return self
     */
    public function __invoke(Document $document = null)
    {
        if ($document) {
            $this->setDocument($document);
        }

        return $this;
    }

    /**
     * Return the Author Document
     * @return Document|null
     */
    public function get()
    {
        $link = $this->getFragment();
        if ($link && $link instanceof DocumentLink) {
            $authorDoc = $this->getContext()->getDocumentById($link->getId());

            return $authorDoc;
        }
    }

    protected function getAuthorFrag($name)
    {
        if ($author = $this->get()) {
            $fragName = sprintf('%s.%s', $author->getType(), $name);
            return $author->get($fragName);
        }
    }

    public function getWebsite()
    {
        if ($link = $this->getAuthorFrag('website')) {
            if ($link instanceof LinkInterface) {
                return $this->getLinkResolver()->resolve($link);
            }
        }
    }

    public function getWebsiteAsMeta()
    {
        if ($url = $this->getWebsite()) {
            return sprintf(
                '<meta itemprop="url" content="%s">',
                $url
            );
        }
    }

    public function getWebsiteAsLink()
    {
        if ($url = $this->getWebsite()) {
            return sprintf(
                '<a itemprop="url" href="%1$s" class="author-url"><span class="author-url-text">%1$s</span></a>',
                $url
            );
        }
    }

    public function getFirstName()
    {
        if($fn = $this->getAuthorFrag('first_name')) {
            return $fn->asText();
        }
    }

    public function getLastName()
    {
        if($ln = $this->getAuthorFrag('last_name')) {
            return $ln->asText();
        }
    }

    public function getName()
    {
        if ($name = $this->getAuthorFrag('name')) {
            return $name->asText();
        }
    }

    protected function _getName()
    {
        if( $name = $this->getName() ) {
            return $name;
        }

        $name = [$this->getFirstName(), $this->getLastName()];
        $name = trim(implode(' ', $name));
        if (!empty($name)) {
            return $name;
        }

        return 'Unknown';
    }

    public function getTwitter()
    {
        if ($handle = $this->getAuthorFrag('twitter')) {
            return trim($handle->asText(), '@');
        }
    }

    public function getTwitterUrl()
    {
        if($handle = $this->getTwitter()) {
            return sprintf('https://twitter.com/%s', $handle);
        }
    }

    public function getTwitterAsLink()
    {
        if ($url = $this->getTwitterUrl()) {
            return sprintf(
                '<a href="%s" itemprop="sameAs" class="author-twitter"><span class="twitter-handle">@%s</span></a>',
                $url,
                $this->getTwitter()
            );
        }
    }

    public function getNameAsString()
    {
        $name = $this->view->escapeHtml($this->_getName());
        if ($author = $this->get()) {
            if ($url = (string) $this->view->prismicUrl($author)) {
                $name = sprintf(
                    '<a href="%s" class="author-posts-link">%s</a>',
                    $url,
                    $name
                );
            }
        }
        return sprintf(
            '<span class="author-name" itemprop="author" itemscope="itemscope" itemtype="http://schema.org/Person">
                <span itemprop="name">%s</span>
                %s
                %s
            </span>',
            $name,
            $this->getTwitterAsLink(),
            $this->getWebsiteAsLink()
        );
    }



}
