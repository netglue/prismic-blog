<?php

namespace NetgluePrismicBlog\View\Helper;

use NetgluePrismicBlog\Service\MetaDataService;
use NetgluePrismicBlog\Service\MetaDataServiceAwareTrait;
use NetgluePrismicBlog\Service\MetaDataServiceAwareInterface;
use Zend\View\Helper\AbstractHelper;
use Zend\Tag\Cloud;

class TagCloud extends AbstractHelper implements MetaDataServiceAwareInterface
{

    use MetaDataServiceAwareTrait;

    private $route = 'prismic-blog/tag';

    private $routeTagParam = 'tag';

    public function __construct(MetaDataService $service)
    {
        $this->setMetaDataService($service);
    }

    public function __invoke()
    {
        return $this;
    }

    public function getTags()
    {
        $tags = $this->getMetaDataService()->getTags();
        foreach($tags as $tag => $count) {
            $url = $this->view->url(
                $this->route,
                [$this->routeTagParam => $tag]
            );
            $tags[$tag] = [
                'title' => $tag,
                'weight' => $count,
                'params' => [
                    'url' => $url,
                ],
            ];
        }
        return $tags;
    }

    public function __toString()
    {
        $cloud = new Cloud(['tags' => $this->getTags()]);
        $cloud->getTagDecorator()
            ->setMaxFontSize(160)
            ->setMinFontSize(100)
            ->setFontSizeUnit('%');
        $cloud->getCloudDecorator()
            ->setHtmlTags(['ul' => ['class' => 'article-tag-cloud']]);
        return (string) $cloud;
    }
}
