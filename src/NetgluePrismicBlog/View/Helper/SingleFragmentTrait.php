<?php

namespace NetgluePrismicBlog\View\Helper;

use NetgluePrismicDefaults\Common\SetDocumentTrait;

trait SingleFragmentTrait
{

    use SetDocumentTrait;

    private $fragmentName;

    /**
     * Set fragment name we're looking for
     * @param string $name
     * @return self
     */
    public function setFragmentName($name)
    {
        $this->fragmentName = (string) $name;

        return $this;
    }

    protected function getFragmentName()
    {
        if($doc = $this->getDocument()) {
            if(!empty($this->fragmentName)) {
                return strpos($this->fragmentName, '.') !== false
                    ? $this->fragmentName
                    : sprintf('%s.%s', $doc->getType(), $this->fragmentName);
            }
        }
    }

    protected function getFragment()
    {
        if($name = $this->getFragmentName()) {
            return $this->getDocument()->get($name);
        }
    }


}
