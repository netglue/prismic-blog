<?php

namespace NetgluePrismicBlog\View\Helper;

use Prismic\Document;
use Zend\View\Helper\AbstractHelper;
use DateTime;
use Carbon\Carbon;

class PostDate extends AbstractHelper
{

    use SingleFragmentTrait;

    /**
     * @var bool
     */
    private $includeSchema = true;

    /**
     * Schema itemprop
     * @var string
     */
    private $schemaProp = 'datePublished';

    /**
     * Whether to default to now if the date can't be found
     * @var bool
     */
    private $defaultNow = false;

    public function __construct()
    {
        $this->setFragmentName('published');
    }

    /**
     * @param Document $document
     * @return self
     */
    public function __invoke(Document $document = null)
    {
        if ($document) {
            $this->setDocument($document);
        }

        return $this;
    }

    /**
     * Whether or not schema data should be included
     * @param bool $flag
     * @return self
     */
    public function includeSchema($flag = true)
    {
        $this->includeSchema = (bool) $flag;

        return $this;
    }

    /**
     * Whether time values default to now, or we get an empty string
     * @param bool $flag
     * @return self
     */
    public function defaultNow($flag = false)
    {
        $this->defaultNow = (bool) $flag;

        return $this;
    }

    /**
     * Set the schema property to use
     * @param string $name
     * @return self
     */
    public function setSchemaProp($name)
    {
        $this->schemaProp = (string) $name;

        return $this;
    }

    /**
     * Return the date as a meta element
     * @return string
     */
    public function asMeta()
    {
        if ($date = $this->getDate()) {
            return sprintf(
                '<meta itemprop="%s" content="%s">',
                $this->schemaProp,
                $date->format('c')
            );
        }

        return '';
    }

    /**
     * Return the date as a time element using the given format for date
     * @param string $format
     * @return string
     */
    public function asTime($format = 'jS F Y')
    {
        if ($date = $this->getDate()) {
            $schemaAttr = $this->includeSchema
                ? sprintf('itemprop="%s"', $this->schemaProp)
                : '';

            return sprintf(
                '<time datetime="%s" %s>%s</time>',
                $date->format('c'),
                $schemaAttr,
                $date->format($format)
            );
        }

        return '';
    }

    /**
     * Return a time element formatted as a relative date
     * @return string
     */
    public function asRelativeDate()
    {
        if ($date = $this->getDate()) {
            $carbon = Carbon::instance($date);
            $schemaAttr = $this->includeSchema
                ? sprintf('itemprop="%s"', $this->schemaProp)
                : '';

            return sprintf(
                '<time datetime="%s" %s>%s</time>',
                $date->format('c'),
                $schemaAttr,
                $carbon->diffForHumans()
            );
        }

        return '';
    }

    /**
     * Return the Date Object
     * @return DateTime|null
     */
    public function getDate()
    {
        if($frag = $this->getFragment()) {
            return $frag->asDateTime();
        }
        if($this->defaultNow) {
            return new DateTime;
        }
    }
}
