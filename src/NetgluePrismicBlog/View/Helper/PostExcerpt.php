<?php

namespace NetgluePrismicBlog\View\Helper;

use Prismic\Document;
use Prismic\Fragment;
use Prismic\WithFragments;

use Zend\View\Helper\AbstractHelper;
use NetgluePrismicBlog\Service\BlogConfigAwareTrait;
use NetgluePrismicDefaults\Common\SetDocumentTrait;
use NetgluePrismicDefaults\Common\LinkResolverAwareTrait;

class PostExcerpt extends AbstractHelper
{

    use SetDocumentTrait;
    use LinkResolverAwareTrait;
    use BlogConfigAwareTrait;

    private $customExcerptFragmentName = 'excerpt';

    private $bodyFragmentName = 'body';

    private $excerptLength;

    /**
     * @param Document $document
     * @return self
     */
    public function __invoke(Document $document = null)
    {
        if ($document) {
            $this->setDocument($document);
        }

        return $this;
    }

    public function setExcerptLength($length)
    {
        $this->excerptLength = (int) $length;

        return $this;
    }

    public function getExcerptLength()
    {
        if(!$this->excerptLength) {
            $length = $this->getBlogConfig()->get('excerpt_length');
            if(!$length) {
                $length = 50;
            }
            return $length;
        }

        return $this->excerptLength;
    }

    protected function getFragment($name)
    {
        if ($doc = $this->getDocument()) {
            $type = $doc->getType();
            $fragName = strpos($name, '.') !== false
                      ? $name
                      : sprintf('%s.%s', $type, $name);

            return $doc->get($fragName);
        }
    }

    public function setCustomExcerptFragmentName($name)
    {
        $this->customExcerptFragmentName = (string) $name;

        return $this;
    }

    public function getCustomExcerptFragment()
    {
        if ($this->customExcerptFragmentName) {
            return $this->getFragment($this->customExcerptFragmentName);
        }
    }

    public function getCustomExcerpt()
    {
        if($fragment = $this->getCustomExcerptFragment()) {
            return $fragment->asHtml($this->getLinkResolver());
        }
    }

    public function setBodyFragmentName($name)
    {
        $this->bodyFragmentName = (string) $name;

        return $this;
    }

    public function getBodyFragment()
    {
        if ($this->bodyFragmentName) {
            return $this->getFragment($this->bodyFragmentName);
        }
    }

    public function getBody()
    {
        if ($body = $this->getBodyFragment()) {
            if($body instanceof Fragment\SliceZone) {
                $frags = $this->extractSliceZone($body);
            } else {
                $frags = $this->extractFragments($body);
            }
            if (count($frags)) {
                $out = '';
                foreach($frags as $frag) {
                    $out .= $frag->asText() . ' ';
                }
                $words = explode(' ', $out);
                array_walk($words, function($val) {
                    return trim($val);
                });
                $words = array_filter($words, function($val) {
                    return !empty($val);
                });

                $words = array_slice($words, 0, $this->getExcerptLength());
                return sprintf(
                    '<p>%s…</p>',
                    $this->view->escapeHtml(implode(' ', $words))
                );
            }
        }
    }

    public function toString()
    {
        if($excerpt = $this->getCustomExcerpt()) {
            return $excerpt;
        }

        return (string) $this->getBody();
    }

    public function __toString()
    {
        return $this->toString();
    }

    protected function extractSliceZone(Fragment\SliceZone $zone)
    {
        $frags = [];
        foreach($zone->getSlices() as $slice) {
            $frags = array_merge($frags, $this->extractFragments($slice->getValue()));
        }
        return $frags;
    }

    protected function extractFragments(Fragment\FragmentInterface $frag)
    {
        $frags = [];
        if($frag instanceof Fragment\Group) {
            foreach($frag->getArray() as $groupDoc) {
                $frags = array_merge($frags, $groupDoc->getFragments());
            }
        } else {
            $frags[] = $frag;
        }

        return $this->filterFragments($frags);
    }

    protected function filterFragments(array $fragments)
    {
        $frags = [];
        foreach($fragments as $frag) {
            if($frag instanceof Fragment\StructuredText) {
                $frags[] = $frag;
            }
        }
        return $frags;
    }

}
