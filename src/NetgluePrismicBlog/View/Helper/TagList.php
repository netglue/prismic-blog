<?php
namespace NetgluePrismicBlog\View\Helper;
use NetgluePrismicBlog\Service\MetaDataService;
use NetgluePrismicBlog\Service\MetaDataServiceAwareTrait;
use NetgluePrismicBlog\Service\MetaDataServiceAwareInterface;
use NetgluePrismicBlog\Service\BlogConfigAwareTrait;
use NetgluePrismicBlog\Service\BlogConfig as Config;
use Zend\View\Helper\AbstractHelper;
use Prismic\Document;

class TagList extends AbstractHelper implements MetaDataServiceAwareInterface
{

    use MetaDataServiceAwareTrait;
    use BlogConfigAwareTrait;

    private $route = 'prismic-blog/tag';

    public function __construct(MetaDataService $service, Config $config)
    {
        $this->setMetaDataService($service);
        $this->setBlogConfig($config);
    }

    public function __invoke()
    {
        return $this;
    }

    public function listAll($count = null)
    {
        $count = $count ? (int) $count : $this->getConfiguredMax();
        $tags = $this->getMetaDataService()->getTags();

        $list = array_slice($tags, 0, $count, true);
        return $this->renderArray($list);
    }

    /**
     * Return either a tag count for the given article, or a count of all tags
     * @param Document $document
     * @return int
     */
    public function count(Document $document = null)
    {
        if($document) {
            return count($document->getTags());
        }

        return count($this->getMetaDataService()->getTags());
    }

    public function article(Document $document)
    {
        $tags = $document->getTags();
        $all = $this->getMetaDataService()->getTags();
        $list = [];
        foreach($all as $tag => $count) {
            if(in_array($tag, $tags, true)) {
                $list[$tag] = $count;
            }
        }
        return $this->renderArray($list);
    }

    private function renderArray($tags)
    {
        if(!count($tags)) {
            return '';
        }
        $list = [];
        foreach($tags as $tag => $tagCount) {
            $list[] = $this->getTag($tag, $tagCount);
        }
        return sprintf('<ul class="tag-list">%s</ul>', implode("\n", $list));
    }

    private function getTag($name, $count)
    {
        $url = $this->view->url($this->route, ['tag' => $name]);
        return sprintf(
            '<li><a href="%s">%s<span class="count">%d</span></a></li>',
            $url,
            $this->view->escapeHtml($name),
            $count
        );
    }

    public function getConfiguredMax()
    {
        $max = $this->getBlogConfig()->get('tag_cloud_max');
        return $max ? (int) $max : 10;
    }

}
