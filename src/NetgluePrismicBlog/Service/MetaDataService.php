<?php

namespace NetgluePrismicBlog\Service;

use stdClass;
use NetgluePrismicDefaults\Service\AbstractDocumentService;
use Prismic\Predicates;
use Prismic\Document;

class MetaDataService extends AbstractDocumentService
{

    /**
     * @var string
     */
    private $dataFile;

    /**
     * @var array
     */
    private $data;

    /**
     * The fragment name that signifies publication date
     * @var string|null
     */
    private $pubDate;

    public function __construct($dataFile)
    {
        $this->dataFile = $dataFile;
    }

    /**
     * Set the fragment name that represents publication date
     * @param string $fragment
     * @return void
     */
    public function setPubDateFragment($fragment)
    {
        $this->pubDate = $fragment;
    }

    /**
     * Get the fragment name that represents publication date
     * @return string|null
     */
    public function getPubDateFragment()
    {
        return $this->normaliseFragmentName($this->pubDate);
    }

    /**
     * Return an array containing the years and months where blog posts exist
     *
     * @return array
     */
    public function getArchiveDates()
    {
        $this->getData();
        return isset($this->data['dates']) ? $this->data['dates'] : [];
    }

    /**
     * Loops over all documents and creates a date entry then stores and saves
     * the data
     * @return void
     */
    public function buildArchiveDates()
    {
        $this->getData();
        $dates = [];
        foreach($this->retrieveDocuments() as $document) {
            if ($date = $this->getDocumentDate($document)) {
                $m = (int) $date->format('n');
                $y = (int) $date->format('Y');
                if (!isset($dates[$y])) {
                    $dates[$y] = [];
                }
                if(!isset($dates[$y][$m])) {
                    $dates[$y][$m] = 0;
                }
                $dates[$y][$m]++;
            }
        }

        ksort($dates); // Sort by year ascending
        foreach($dates as $y => $mnths) {
            ksort($dates[$y]); // Sort by month ascending
        }
        $this->data['dates'] = $dates;
        $this->storeData();
    }

    /**
     * Build the tag meta data
     * @return void
     */
    public function buildTagMeta()
    {
        $this->getData();
        $tags = [];
        foreach($this->retrieveDocuments() as $document) {
            foreach($document->getTags() as $tag) {
                if(!isset($tags[$tag])) {
                    $tags[$tag] = 0;
                }
                $tags[$tag]++;
            }
        }
        asort($tags);
        $tags = array_reverse($tags, true);
        $this->data['tags'] = $tags;
        $this->storeData();
    }

    /**
     * Return the tags
     * @return array
     */
    public function getTags()
    {
        $this->getData();
        return isset($this->data['tags']) ? $this->data['tags'] : [];
    }

    /**
     * Retrieve pub date from the given document or null if it can't be found
     * @param Document $document
     * @return \DateTime|null
     */
    private function getDocumentDate(Document $document)
    {
        if ($frag = $document->get($this->getPubDateFragment())) {
            if(method_exists($frag, 'asDateTime')) {
                return $frag->asDateTime();
            }
        }
    }

    /**
     * Return the prismic response containing the documents in the given page
     * @param  int      $page
     * @return Response
     */
    private function retrieveDocumentsByPage($page)
    {
        $api        = $this->getPrismicApi();
        $ref        = $this->getContext()->getRef();
        $predicates = array(
            Predicates::any("document.type", [$this->getDocumentType()]),
        );
        $form = $api->forms()->everything
                ->ref($ref)
                ->pageSize(100)
                ->page($page)
                ->query($predicates);

        return $form->submit();
    }

    /**
     * Return *all* documents of the configured type
     * @return array An array of Document instances
     */
    private function retrieveDocuments()
    {
        $response = $this->retrieveDocumentsByPage(1);
        $documents = $response->getResults();
        while ($response->getPage() < $response->getTotalPages()) {
            $page = $response->getPage() + 1;
            $response = $this->retrieveDocumentsByPage($page);
            $documents = array_merge($documents, $response->getResults());
        }

        return $documents;
    }

    /**
     * @return array
     */
    public function getData()
    {
        if (!$this->data) {
            $this->loadData();
        }

        return $this->data;
    }

    /**
     * @return array
     */
    private function loadData()
    {
        $this->data = [];
        if ($this->dataFile && file_exists($this->dataFile)) {
            $this->data = json_decode(file_get_contents($this->dataFile), true);
        }

        return $this->data;
    }

    /**
     * Writes archive data/dates to the configured location
     * @return bool
     */
    private function storeData()
    {
        return file_put_contents($this->dataFile, json_encode($this->data, true));
    }

}
