<?php

namespace NetgluePrismicBlog\Service;

use Prismic\Document;
use Prismic\Fragment\GeoPoint;
use Prismic\Fragment\Image;
use Prismic\Fragment\Link\LinkInterface;
use Prismic\LinkResolver;

class BlogConfig
{
    /**
     * @var Document
     */
    private $document;

    /**
     * @var LinkResolver
     */
    private $linkResolver;

    /**
     * @var string
     */
    private $mask;

    /**
     * Requires a Prismic Document to be used as the configuration store
     * @param Document $document
     * @param LinkResolver $linkResolver
     * @return void
     */
    public function __construct(Document $document, LinkResolver $linkResolver)
    {
        $this->document = $document;
        $this->linkResolver = $linkResolver;
        $this->mask = $this->document->getType();
    }

    /**
     * Prefix fragment name with config document type
     * @param string $name
     * @return string
     * @throws \RuntimeException if you try to retrieve a fragment already prefixed with a name that doesn't match the config document type
     */
    private function normaliseFragment($name)
    {
        if (strpos($name, $this->mask) === 0) {
            return $name;
        }
        if (strpos($name, '.') !== false) {
            throw new \RuntimeException(sprintf(
                'Found a dot in the fragment name [%s] but does not match configured mask/type of %s',
                $name,
                $this->mask
            ));
        }
        return $this->mask . '.' . $name;
    }

    /**
     * Return the fragment identifed by name
     * @param string $name
     * @return \Prismic\Fragment\FragmentInterface|null
     */
    public function getFragment($name)
    {
        return $this->document->get(
            $this->normaliseFragment($name)
        );
    }

    /**
     * Return text value of the fragment identified by name
     * @param string $name
     * @return string|null
     */
    public function get($name)
    {
        $frag = $this->getFragment($name);
        if ($frag !== null) {
            return $frag->asText();
        }
        return null;
    }

    /**
     * Return HTML value of the fragment identified by name
     * @param string $name
     * @return string|null
     */
    public function getHtml($name)
    {
        $frag = $this->getFragment($name);
        if ($frag !== null) {
            return $frag->asHtml($this->linkResolver);
        }
        return null;
    }

    /**
     * Return the URL value of the fragment identified by name
     * @param string $name
     * @return string|null
     */
    public function getUrl($name)
    {
        $link = $this->getFragment($name);
        if ($link instanceof LinkInterface) {
            return $this->linkResolver->resolve($link);
        }

        return null;
    }

    /**
     * Return latitude of the geopoint fragment identified by name
     * @param string $name
     * @return string|null
     */
    public function getLatitude($name)
    {
        $frag = $this->getFragment($name);
        if ($frag instanceof GeoPoint) {
            return $frag->getLatitude();
        }
        return null;
    }

    /**
     * Return longitude of the geopoint fragment identified by name
     * @param string $name
     * @return string|null
     */
    public function getLongitude($name)
    {
        $frag = $this->getFragment($name);
        if ($frag instanceof GeoPoint) {
            return $frag->getLongitude();
        }
        return null;
    }

    /**
     * Return the image URL of the fragment identified by name
     * @param string $name
     * @return string
     */
    public function getImageUrl($name)
    {
        $frag = $this->getFragment($name);
        if ($frag instanceof Image) {
            return $frag->getMain()->getUrl();
        }
        return null;
    }
}
