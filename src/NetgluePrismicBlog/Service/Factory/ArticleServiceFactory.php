<?php

namespace NetgluePrismicBlog\Service\Factory;

use NetgluePrismicBlog\Service\ArticleService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ArticleServiceFactory implements FactoryInterface
{
    /**
     * Return ArticleService
     * @param ServiceLocatorInterface $serviceLocator
     * @return ArticleService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config  = $serviceLocator->get('Config');
        $context = $serviceLocator->get('Prismic\Context');
        $api     = $serviceLocator->get('Prismic\Api');
        $repo    = new ArticleService;

        $repo->setContext($context);
        $repo->setPrismicApi($api);
        $repo->setDocumentType($config['prismic']['blog']['documentType']);
        $repo->setDefaultOrder($config['prismic']['blog']['defaultOrder']);
        $repo->setPubDateFragment($config['prismic']['blog']['pubDate']);
        $repo->setBlogConfig($serviceLocator->get('NetgluePrismicBlog\Service\BlogConfig'));
        return $repo;
    }
}
