<?php

namespace NetgluePrismicBlog\Service\Factory;

use NetgluePrismicBlog\Service\MetaDataService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use NetgluePrismicBlog\Exception\BlogException;

class MetaDataServiceFactory implements FactoryInterface
{
    /**
     * Return MetaDataService
     * @param ServiceLocatorInterface $serviceLocator
     * @return MetaDataService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config  = $serviceLocator->get('Config');
        $context = $serviceLocator->get('Prismic\Context');
        $api     = $serviceLocator->get('Prismic\Api');

        if(!isset($config['prismic']['blog']['meta-data-file'])) {
            throw BlogException::metaDataFileNotSet();
        }

        $repo = new MetaDataService($config['prismic']['blog']['meta-data-file']);
        $repo->setContext($context);
        $repo->setPrismicApi($api);
        $repo->setDocumentType($config['prismic']['blog']['documentType']);
        $repo->setPubDateFragment($config['prismic']['blog']['pubDate']);
        return $repo;
    }
}
