<?php

namespace NetgluePrismicBlog\Service\Factory;

use NetgluePrismic\Exception\ExceptionInterface as PrismicException;
use NetgluePrismicBlog\Service\BlogConfig;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BlogConfigFactory implements FactoryInterface
{
    /**
     * Return BlogConfig
     * @param ServiceLocatorInterface $serviceLocator
     * @return BlogConfig
     * @throws \RuntimeException
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');

        if (!isset($config['prismic']['blog']['config-bookmark'])) {
            throw new \RuntimeException('Cannot initialise blog config unless a bookmark has been configured');
        }

        try {
            $context = $serviceLocator->get('Prismic\Context');
            $document = $context->getDocumentByBookmark($config['prismic']['blog']['config-bookmark']);
        } catch (PrismicException $e) {
            throw new \RuntimeException('Failed to load blog configuration bookmark from repository', null, $e);
        }

        $linkResolver = $serviceLocator->get('NetgluePrismic\Mvc\LinkResolver');

        $blog = new BlogConfig($document, $linkResolver);
        return $blog;
    }
}
