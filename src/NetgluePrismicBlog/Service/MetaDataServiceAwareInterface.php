<?php

namespace NetgluePrismicBlog\Service;

interface MetaDataServiceAwareInterface
{

    /**
     * @param  MetaDataService $service
     * @return null
     */
    public function setMetaDataService(MetaDataService $service);

    /**
     * @return MetaDataService|null
     */
    public function getMetaDataService();

}
