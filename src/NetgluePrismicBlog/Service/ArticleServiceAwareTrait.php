<?php

namespace NetgluePrismicBlog\Service;

trait ArticleServiceAwareTrait
{

    /**
     * @var ArticleService
     */
    protected $articleService;

    /**
     * @param  ArticleService $service
     * @return null
     */
    public function setArticleService(ArticleService $service)
    {
        $this->articleService = $service;
    }

    /**
     * @return ArticleService|null
     */
    public function getArticleService()
    {
        return $this->articleService;
    }

}
