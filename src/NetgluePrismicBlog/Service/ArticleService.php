<?php

namespace NetgluePrismicBlog\Service;

use NetgluePrismicDefaults\Service\AbstractDocumentService;
use NetgluePrismicBlog\Exception;
use Zend\Paginator\Paginator;
use NetgluePrismic\Paginator\Adapter\SearchFormAdapter;
use Prismic\Predicates;
use Prismic\SearchForm;
use Prismic\Document;
use NetgluePrismicBlog\Service\BlogConfigAwareTrait;
use DateTime;
use DateInterval;

class ArticleService extends AbstractDocumentService
{

    use BlogConfigAwareTrait;

    /**
     * The fragment name that signifies publication date
     * @var string|null
     */
    private $pubDate;

    /**
     * A final fallback page size if it can't be figured out anywhere else
     * @var int
     */
    public static $fallbackPageSize = 10;

    /**
     * When finding the next/prev document - how much should be added to or subtracted from the document date value?
     *
     * This is documented here because if the Pub Date fragment is a Date, rather than a Timestamp,
     * then a whole day must be used, rather than say a single second to created a datetime 'after' or 'before'
     * that of the document concerned. Haven't figured out how to best handle the possibility that I don't know
     * whether the frag is a date or timestamp. I guess, timestamp is best practice, as then you could have more than one
     * post per day. With a regular date, only one post per day is plausible/accurate.
     * That's really shit.
     */
    private $dateIntervalSpec = 'P1D';

    /**
     * A DateTime instance representing tomorrow morning
     *
     * The reason for this is to use a consistent timestamp in queries, rather than now,
     * so that caches can take advantage as the URL for the API is often used as the cache key
     * @var DateTime
     */
    private $tomorrow;

    public function __construct()
    {
        $date = new DateTime;
        $date->setTime(0,0,0);
        $date->add(new DateInterval('P1D'));
        $this->tomorrow = $date;
    }

    /**
     * Required Features
     *
     * Return oldest article on record
     * Return *n* most recent articles
     */

    /**
     * Set the fragment name that represents publication date
     * @param string $fragment
     * @return void
     */
    public function setPubDateFragment($fragment)
    {
        $this->pubDate = $fragment;
    }

    /**
     * Get the fragment name that represents publication date
     * @return string|null
     */
    public function getPubDateFragment()
    {
        return $this->normaliseFragmentName($this->pubDate);
    }


    /**
     * Return articles that were published in the given year/month
     * @param int $year
     * @param int $month
     * @return Paginator
     */
    public function findByMonth($year, $month, $perPage = null)
    {
        // Easier to use timestamps:
        $after  = mktime(0, 0, 0, $month, 1, $year) * 1000;
        $before = mktime(0, 0, 0, $month + 1, 1, $year) * 1000;

        $predicates = [
            Predicates::any("document.type", [$this->getDocumentType()]),
            Predicates::dateBetween(
                sprintf("my.%s", $this->getPubDateFragment()),
                $after,
                $before
            ),
        ];

        $order = sprintf('my.%s', $this->getPubDateFragment());

        return $this->getPagedQuery($predicates, $order, $perPage);
    }

    /**
     * Whether the given $tag is valid for finding by tag
     * @param array|string &$tag
     * @return bool
     */
    private function validTagParam(&$arg)
    {
        if(is_string($arg) && !empty($arg)) {
            $arg = [$arg];
            return true;
        }
        if(is_array($arg)) {
            foreach($arg as $k => $v) {
                if(!is_string($v) || empty(trim($v))) {
                    unset($arg[$k]);
                }
            }
            return count($arg) > 0;
        }
        return false;
    }

    /**
     * Locate articles with given tag
     * @param string|array $tag
     * @return Paginator
     */
    public function findByTag($tag, $perPage = null)
    {
        $arg = $tag;
        if(!$this->validTagParam($tag)) {
            throw Exception\InvalidArgumentException::tagNotExpectedArray($arg);
        }

        $predicates = [
            Predicates::any("document.type", [$this->getDocumentType()]),
            Predicates::any("document.tags", $tag),
        ];

        $order = sprintf('my.%s desc', $this->getPubDateFragment());

        return $this->getPagedQuery($predicates, $order, $perPage);
    }

    /**
     * Return all published articles paginated. Newest first
     *
     * @return Paginator
     */
    public function getAllPaged($perPage = null)
    {
        $predicates = [
            Predicates::any("document.type", [$this->getDocumentType()]),
            Predicates::dateBefore(
                sprintf("my.%s", $this->getPubDateFragment()),
                $this->tomorrow
            ),
        ];

        $order = sprintf('my.%s desc', $this->getPubDateFragment());

        return $this->getPagedQuery($predicates, $order, $perPage);
    }

    private function getPagedQuery(array $predicates, $order = null, $perPage = null)
    {
        $api        = $this->getPrismicApi();
        $ref        = $this->getContext()->getRefAsString();
        $form       = $api->forms()->everything
                          ->ref($ref)
                          ->query($predicates);
        if ($order) {
            $form = $form->orderings($order);
        }

        $pager = $this->createPager($form);
        $pager->setItemCountPerPage($this->getPageSize($perPage));

        return $pager;
    }

    /**
     * Return page size
     *
     * Prefers a custom value, if valid as passed in $current
     * Tries to get it from the global config if set, then falls back on a
     * static property if all else fails
     *
     * @param int $current
     * @return int
     */
    protected function getPageSize($current = null)
    {
        if ($current && is_int($current) && $current > 1) {
            return $current;
        }
        $config = $this->getBlogConfig()->get('default_page_size');
        if ($config && is_numeric($config)) {
            return (int) $config;
        }

        return static::$fallbackPageSize;
    }

    /**
     * Create a pager for the given Prismic Search Form
     * @param SearchForm $form
     * @return Paginator
     */
    public function createPager(SearchForm $searchForm)
    {
        $adapter = new SearchFormAdapter($searchForm);
        return new Paginator($adapter);
    }

    /**
     * Make sure that the parameter is the correct document type
     * @param Document $document
     * @return void
     */
    private function assertDocumentType(Document $document)
    {
        if ($document->getType() !== $this->getDocumentType()) {
            throw Exception\InvalidArgumentException::nonMatchingDocType($document->getType(), $this->getDocumentType());
        }
    }

    /**
     * Return the publication date of the given document
     * @param Document $document
     * @return DateTime|null
     */
    private function getPubDate(Document $document)
    {
        $this->assertDocumentType($document);

        $frag = $this->getPubDateFragment();
        if ($date = $document->get($frag)) {
            return $date->asDateTime();
        }
    }

    public function getOlder(Document $document)
    {
        if ($date = $this->getPubDate($document)) {
            $predicates = [
                Predicates::any("document.type", [$this->getDocumentType()]),
                Predicates::dateBefore(
                    sprintf("my.%s", $this->getPubDateFragment()),
                    $date
                ),
            ];
            $order = sprintf('my.%s desc', $this->getPubDateFragment());
            $response = $this->query($predicates, $order, 1);
            if (count($response->getResults())) {
                return current($response->getResults());
            }
        }
    }

    public function getNewer(Document $document)
    {
        if ($date = $this->getPubDate($document)) {
            $date->add(new DateInterval($this->dateIntervalSpec));
            $predicates = [
                Predicates::any("document.type", [$this->getDocumentType()]),
                Predicates::dateAfter(
                    sprintf("my.%s", $this->getPubDateFragment()),
                    $date
                ),
                Predicates::dateBefore(
                    sprintf("my.%s", $this->getPubDateFragment()),
                    $this->tomorrow // Before
                ),
            ];
            // Order oldest first
            $order = sprintf('my.%s', $this->getPubDateFragment());
            $response = $this->query($predicates, $order, 1);
            if (count($response->getResults())) {
                return current($response->getResults());
            }
        }
    }


}
