<?php

namespace NetgluePrismicBlog\Service;

trait BlogConfigAwareTrait
{
    /**
     * @var BlogConfig
     */
    private $blogConfig;

    /**
     * Set Config
     *
     * @param BlogConfig $config
     * @return void
     */
    public function setBlogConfig(BlogConfig $config)
    {
        $this->blogConfig = $config;
    }

    /**
     * Return blog config
     *
     * @return BlogConfig|null
     */
    public function getBlogConfig()
    {
        return $this->blogConfig;
    }
}
