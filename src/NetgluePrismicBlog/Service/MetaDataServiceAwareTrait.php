<?php

namespace NetgluePrismicBlog\Service;

trait MetaDataServiceAwareTrait
{

    /**
     * @var MetaDataService
     */
    protected $metaDataService;

    /**
     * @param  MetaDataService $service
     * @return null
     */
    public function setMetaDataService(MetaDataService $service)
    {
        $this->metaDataService = $service;
    }

    /**
     * @return MetaDataService|null
     */
    public function getMetaDataService()
    {
        return $this->metaDataService;
    }

}
