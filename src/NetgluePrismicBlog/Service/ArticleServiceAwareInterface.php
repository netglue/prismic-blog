<?php

namespace NetgluePrismicBlog\Service;

interface ArticleServiceAwareInterface
{

    /**
     * @param  ArticleService $service
     * @return null
     */
    public function setArticleService(ArticleService $service);

    /**
     * @return ArticleService|null
     */
    public function getArticleService();

}
