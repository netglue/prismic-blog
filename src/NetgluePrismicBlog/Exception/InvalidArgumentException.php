<?php

namespace NetgluePrismicBlog\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{

    public static function tagNotExpectedArray($argument)
    {
        $type = gettype($argument);
        $msg = sprintf(
            'Tags should be sanitised prior to retrieval and should be a non-empty string or an array of non-empty strings. Received %s',
            $type
        );
        return new static($msg);

    }

    public static function nonMatchingDocType($given, $expected)
    {
        $msg = sprintf(
            'The document received has the type of \'%s\' whereas I was expecting a document of type \'%s\'',
            $given,
            $received
        );
        return new static($msg);

    }


}
