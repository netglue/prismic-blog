<?php

namespace NetgluePrismicBlog\Exception;

class BlogException extends \Exception implements ExceptionInterface
{

    public static function archiveDataFileNotSet()
    {
        return new static('The archive data file has not been set to a valid path');
    }

}
