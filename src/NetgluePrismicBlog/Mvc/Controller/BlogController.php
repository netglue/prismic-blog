<?php

namespace NetgluePrismicBlog\Mvc\Controller;

use NetgluePrismicDefaults\Mvc\Controller\AbstractPrismicController;
use NetgluePrismicBlog\Service\ArticleService;
use NetgluePrismicBlog\Service\MetaDataService;
use DateTime;
use Zend\Escaper\Escaper;

class BlogController extends AbstractPrismicController
{

    private $repo;

    private $metaService;

    private $escaper;

    public function __construct()
    {
        $this->escaper = new Escaper;
    }

    /**
     * Present a single article
     * @return \Zend\View\ViewModel
     */
    public function articleAction()
    {
        $view = $this->createViewModelFromRoute();

        if (false === $view) {
            return;
        }

        $view->post = $this->getDocument();

        return $view;
    }

    public function indexAction()
    {
        $view = $this->createViewModelFromRoute();

        if (false === $view) {
            return;
        }

        $page = $this->params()->fromRoute('page');
        $view->posts = $this->repo->getAllPaged();
        $view->posts->setCurrentPageNumber($page);
        $view->route = 'prismic-blog/paged';

        return $view;
    }

    public function archivesAction()
    {

        $view = $this->createViewModelFromRoute();
        if (false === $view) {
            return;
        }

        $view->year  = $this->params()->fromRoute('year');
        $view->month = $this->params()->fromRoute('month');
        $page = $this->params()->fromRoute('page');

        if (empty($view->year)) {
            $view->year = date("Y");
        }
        if (empty($view->month)) {
            $view->month = date("n");
        }

        if (!is_numeric($view->year) || !is_numeric($view->month)) {
            $this->raise404();
            return;
        }

        $view->year  = (int) $view->year;
        $view->month = (int) $view->month;

        $view->date  = new DateTime;
        $view->date->setDate($view->year, $view->month, 1);
        $view->date->setTime(12, 0, 0);

        $view->posts = $this->repo->findByMonth($view->year, $view->month);

        /**
         * Setup the ArchiveFilter to filter archive dates
         */
        $sl = $this->getServiceLocator();
        $filterManager = $sl->get('FilterManager');
        $filter = $filterManager->get('NetgluePrismicBlog\Filter\ArchiveFilter');
        $filter->setYear($view->year);
        $filter->setMonth($view->month);

        $view->route = 'prismic-blog/archives';
        $view->posts->setCurrentPageNumber($page);
        return $view;
    }

    protected function filterVar($search, $replace)
    {
        $sl = $this->getServiceLocator();
        $filterManager = $sl->get('FilterManager');
        $filter = $filterManager->get('NetgluePrismicBlog\Filter\ArchiveFilter');
        $filter->addReplacement($search, $replace);
    }

    public function tagAction()
    {
        $view = $this->createViewModelFromRoute();
        if (false === $view) {
            return;
        }
        $view->tag = $this->params()->fromRoute('tag');
        if(empty($view->tag) || !is_string($view->tag)) {
            $this->raise404();
            return;
        }

        $view->posts = $this->repo->findByTag($view->tag);
        $this->filterVar('%tag', $this->escaper->escapeHtml($view->tag));
        $view->route = 'prismic-blog/tag';
        $page = $this->params()->fromRoute('page');
        $view->posts->setCurrentPageNumber($page);
        return $view;
    }

    public function setArticleService(ArticleService $service)
    {
        $this->repo = $service;
    }

    public function setMetaDataService(MetaDataService $service)
    {
        $this->metaService = $service;
    }

    public function getArticleService()
    {
        return $this->repo;
    }
}
