<?php

namespace NetgluePrismicBlog\Mvc\Controller\Plugin\Factory;

use NetgluePrismicBlog\Mvc\Controller\Plugin\BlogConfig;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SiteConfigFactory implements FactoryInterface
{
    /**
     * Return BlogConfig controller plugin
     * @param  ServiceLocatorInterface $controllerPluginManager
     * @return BlogConfig
     */
    public function createService(ServiceLocatorInterface $controllerPluginManager)
    {
        $serviceLocator = $controllerPluginManager->getServiceLocator();
        $config = $serviceLocator->get('NetgluePrismicBlog\Service\BlogConfig');
        return new BlogConfig($config);
    }
}
