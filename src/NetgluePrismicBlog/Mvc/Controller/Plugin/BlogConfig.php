<?php

namespace NetgluePrismicBlog\Mvc\Controller\Plugin;

use NetgluePrismicBlog\Service\BlogConfig;
use NetgluePrismicBlog\Service\BlogConfigAwareTrait;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class SiteConfig extends AbstractPlugin
{

    use BlogConfigAwareTrait;

    /**
     * @param  BlogConfig $config
     * @return void
     */
    public function __construct(Config $config)
    {
        $this->setBlogConfig($config);
    }

    /**
     * Proxy to site-wide config
     */
    public function __call($method, $args)
    {
        return call_user_func_array(array($this->blogConfig, $method), $args);
    }

}
