<?php

namespace NetgluePrismicBlog\Mvc\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use NetgluePrismicBlog\Service\MetaDataService;

class ConsoleController extends AbstractActionController
{
    private $metaService;

    public function setMetaDataService(MetaDataService $service)
    {
        $this->metaService = $service;
    }

    public function rebuildArchivesAction()
    {
        $this->metaService->buildArchiveDates();
    }

    public function rebuildTagsAction()
    {
        $this->metaService->buildTagMeta();
    }

}
