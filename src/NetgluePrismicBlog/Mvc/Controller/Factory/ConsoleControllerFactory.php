<?php

namespace NetgluePrismicBlog\Mvc\Controller\Factory;

use NetgluePrismicBlog\Mvc\Controller\ConsoleController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConsoleControllerFactory implements FactoryInterface
{
    /**
     * Return ConsoleController
     * @param  ServiceLocatorInterface $controllerManager
     * @return ConsoleController
     */
    public function createService(ServiceLocatorInterface $controllerManager)
    {
        $serviceLocator = $controllerManager->getServiceLocator();

        $controller = new ConsoleController;
        $controller->setMetaDataService($serviceLocator->get('NetgluePrismicBlog\Service\MetaDataService'));
        return $controller;
    }
}
