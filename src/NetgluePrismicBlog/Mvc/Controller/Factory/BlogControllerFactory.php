<?php

namespace NetgluePrismicBlog\Mvc\Controller\Factory;

use NetgluePrismicBlog\Mvc\Controller\BlogController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BlogControllerFactory implements FactoryInterface
{
    /**
     * Return Blog Controller
     * @param  ServiceLocatorInterface $controllerManager
     * @return BlogController
     */
    public function createService(ServiceLocatorInterface $controllerManager)
    {
        $serviceLocator = $controllerManager->getServiceLocator();

        $controller = new BlogController;
        $controller->setArticleService($serviceLocator->get('NetgluePrismicBlog\Service\ArticleService'));
        $controller->setMetaDataService($serviceLocator->get('NetgluePrismicBlog\Service\MetaDataService'));
        return $controller;
    }
}
