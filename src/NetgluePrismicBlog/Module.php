<?php

namespace NetgluePrismicBlog;

use Zend\Console\Adapter\AdapterInterface as Console;
use Zend\Console\Request as ConsoleRequest;
use Zend\EventManager\EventInterface;
use Zend\Http\Request as HttpRequest;
use Zend\Loader\AutoloaderFactory;
use Zend\Loader\StandardAutoloader;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ConsoleBannerProviderInterface;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\ModuleManager\Feature\FilterProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Mvc\MvcEvent;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ControllerProviderInterface,
    ServiceProviderInterface,
    DependencyIndicatorInterface,
    ControllerPluginProviderInterface,
    ViewHelperProviderInterface,
    ConsoleBannerProviderInterface,
    ConsoleUsageProviderInterface,
    FilterProviderInterface,
    BootstrapListenerInterface
{
    /**
     * Expected to return an array of modules on which the current one depends on
     *
     * @return array
     */
    public function getModuleDependencies()
    {
        return [
            'NetgluePrismic',
            'NetgluePrismicDefaults',
        ];
    }

    /**
     * Return autoloader configuration
     * @link http://framework.zend.com/manual/2.0/en/user-guide/modules.html
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return [
            AutoloaderFactory::STANDARD_AUTOLOADER => [
                StandardAutoloader::LOAD_NS => [
                    __NAMESPACE__ => __DIR__,
                ],
            ],
        ];
    }

    /**
     * Include/Return module configuration
     * @return array
     * @implements ConfigProviderInterface
     */
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    /**
     * Bootstrap Listener
     *
     * @param MvcEvent $event
     * @return void
     */
    public function onBootstrap(EventInterface $event)
    {
        if ($event->getRequest() instanceof HttpRequest) {
            $this->attachViewFilters($event);
            $this->disqusCommentCount($event);
        }
    }

    private function attachViewFilters(MvcEvent $event)
    {
        $application    = $event->getApplication();
        $serviceLocator = $application->getServiceManager();
        $renderer       = $serviceLocator->get('Zend\View\Renderer\PhpRenderer');
        $filterChain    = $renderer->getFilterChain();
        $filterManager  = $serviceLocator->get('FilterManager');

        $filters = [
            'NetgluePrismicBlog\Filter\ArchiveFilter',
        ];

        foreach ($filters as $filterName) {
            $filter = $filterManager->get($filterName);
            $filterChain->attach($filter);
        }
    }

    private function disqusCommentCount(MvcEvent $event)
    {
        $application    = $event->getApplication();
        $serviceLocator = $application->getServiceManager();



        $config = $serviceLocator->get('NetgluePrismicBlog\Service\BlogConfig');
        if($shortname = $config->get('disqus_shortname')) {
            if(!empty($shortname)) {
                $renderer = $serviceLocator->get('Zend\View\Renderer\PhpRenderer');
                $plugin   = $renderer->plugin('inlineScript');
                $plugin->setAllowArbitraryAttributes(true);
                $plugin->appendFile(
                    sprintf('//%s.disqus.com/count.js', $shortname),
                    'text/javascript',
                    ['async' => 'async', 'id' => 'dsq-count-scr']
                );
            }
        }
    }

    /**
     * Return Controller Config
     * @return array
     */
    public function getControllerConfig()
    {
        return [
            'factories' => [
                'NetgluePrismicBlog\Mvc\Controller\BlogController'    => 'NetgluePrismicBlog\Mvc\Controller\Factory\BlogControllerFactory',
                'NetgluePrismicBlog\Mvc\Controller\ConsoleController' => 'NetgluePrismicBlog\Mvc\Controller\Factory\ConsoleControllerFactory',
            ],
        ];
    }

    /**
     * Return controller plugin config
     * @return array
     * @implements ControllerPluginProviderInterface
     */
    public function getControllerPluginConfig()
    {
        return [
            'factories' => [
                'NetgluePrismicBlog\Mvc\Controller\Plugin\BlogConfig' => 'NetgluePrismicBlog\Mvc\Controller\Plugin\Factory\BlogConfigFactory',
            ],
            'aliases' => [
                'blogConfig' => 'NetgluePrismicBlog\Mvc\Controller\Plugin\BlogConfig',
            ],
        ];
    }

    /**
     * Return Service Config
     * @return array
     * @implements ServiceProviderInterface
     */
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'NetgluePrismicBlog\Service\BlogConfig'      => 'NetgluePrismicBlog\Service\Factory\BlogConfigFactory',
                'NetgluePrismicBlog\Service\ArticleService'  => 'NetgluePrismicBlog\Service\Factory\ArticleServiceFactory',
                'NetgluePrismicBlog\Service\MetaDataService' => 'NetgluePrismicBlog\Service\Factory\MetaDataServiceFactory',
            ],
        ];
    }

    /**
     * Return view helper plugin config
     * @return array
     * @implements ViewHelperProviderInterface
     */
    public function getViewHelperConfig()
    {
        return [
            'invokables' => [
                'NetgluePrismicBlog\View\Helper\PostDate'        => 'NetgluePrismicBlog\View\Helper\PostDate',
            ],
            'factories' => [
                'NetgluePrismicBlog\View\Helper\PostAuthor'      => 'NetgluePrismicBlog\View\Helper\Factory\PostAuthorFactory',
                'NetgluePrismicBlog\View\Helper\PostExcerpt'     => 'NetgluePrismicBlog\View\Helper\Factory\PostExcerptFactory',
                'NetgluePrismicBlog\View\Helper\ArchiveList'     => 'NetgluePrismicBlog\View\Helper\Factory\ArchiveListFactory',
                'NetgluePrismicBlog\View\Helper\TagCloud'        => 'NetgluePrismicBlog\View\Helper\Factory\TagCloudFactory',
                'NetgluePrismicBlog\View\Helper\TagList'         => 'NetgluePrismicBlog\View\Helper\Factory\TagListFactory',
                'NetgluePrismicBlog\View\Helper\NextArticle'     => 'NetgluePrismicBlog\View\Helper\Factory\NextArticleFactory',
                'NetgluePrismicBlog\View\Helper\PreviousArticle' => 'NetgluePrismicBlog\View\Helper\Factory\PreviousArticleFactory',
                'NetgluePrismicBlog\View\Helper\BlogConfig'      => 'NetgluePrismicBlog\View\Helper\Factory\BlogConfigFactory',
            ],
            'aliases' => [
                'postDate'                                       => 'NetgluePrismicBlog\View\Helper\PostDate',
                'postAuthor'                                     => 'NetgluePrismicBlog\View\Helper\PostAuthor',
                'postExcerpt'                                    => 'NetgluePrismicBlog\View\Helper\PostExcerpt',
                'archiveList'                                    => 'NetgluePrismicBlog\View\Helper\ArchiveList',
                'articleTagCloud'                                => 'NetgluePrismicBlog\View\Helper\TagCloud',
                'articleTagList'                                 => 'NetgluePrismicBlog\View\Helper\TagList',
                'nextArticle'                                    => 'NetgluePrismicBlog\View\Helper\NextArticle',
                'previousArticle'                                => 'NetgluePrismicBlog\View\Helper\PreviousArticle',
                'blogConfig'                                     => 'NetgluePrismicBlog\View\Helper\BlogConfig',
            ],
        ];
    }

    /**
     * Return filter config
     * @return array
     * @implements FilterProviderInterface
     */
    public function getFilterConfig()
    {
        return [
            'invokables' => [
                'NetgluePrismicBlog\Filter\ArchiveFilter' => 'NetgluePrismicBlog\Filter\ArchiveFilter',
            ],
            'factories' => [

            ],
            'aliases' => [

            ],
            'shared' => [
                'NetgluePrismicBlog\Filter\ArchiveFilter' => 'NetgluePrismicBlog\Filter\ArchiveFilter',
            ],
        ];
    }

    /**
     * Return console usage message
     * @param Console $console
     * @return array
     */
    public function getConsoleUsage(Console $console)
    {
        return array(
            'blog rebuild-archives' => 'Rebuild the index of archive dates',
            'blog rebuild-tags'     => 'Rebuild the tag index and tag count',
        );
    }

    /**
     * Return console banner
     * @param Console $console
     * @return array
     */
    public function getConsoleBanner(Console $console)
    {
        return 'Net Glue Prismic Blog';
    }
}
