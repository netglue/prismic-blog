<?php

namespace NetgluePrismicBlog\Filter;

use Zend\Filter\FilterInterface;
use DateTime;

class ArchiveFilter implements FilterInterface
{

    private $year;

    private $month;

    private $replacements = [];

    public function addReplacement($search, $replace)
    {
        $this->replacements[] = [
            'search' => $search,
            'replace' => $replace,
        ];
    }

    public function setYear($year)
    {
        $this->year = $year;
    }

    public function setMonth($month)
    {
        $this->month = $month;
    }

    private function getSearch()
    {
        return [
            '%monthName',
            '%monthShort',
            '%month',
            '%year',
        ];
    }

    private function getReplace()
    {
        $date = new DateTime;
        $date->setDate($this->year, $this->month, 1);
        return [
            $date->format('F'),
            $date->format('M'),
            $date->format('n'),
            $date->format('Y'),
        ];
    }

    /**
     * {@inheritdocs}
     */
    public function filter($value)
    {
        $value = str_replace($this->getSearch(), $this->getReplace(), $value);
        foreach($this->replacements as $pair) {
            $value = str_replace($pair['search'], $pair['replace'], $value);
        }
        return $value;
    }


}
